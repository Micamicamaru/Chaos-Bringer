Blanket Academy Characters



Adrien

	The main character of the story. A 28 year old self taught fantasy artist. He mainly does small gigs for small game studios or does some freelancing of posters or his peronsal art. At the time he just got off working for a semi large project that is  keeping him afloat at the moment, but fund are quickly running out. He is a kind guy, but has a pessemistic side. He likes to joke around , but tries to keep it base. While he has a strong work ethic, he tends to relax a little to much sometimes on other tings besides  working on his art. His art, which he has taught himself using some videos and some help from other people  has developed but he has never had formal training. This hinders him as his style has developed, but his knowledge and understanding has been stunnted. His art as an odd quality that no one seems to know what is , even himself can see and still cant correct it. He yearns for a mentor to help him with his art, or at least someone that could help him bounce ideas off and point him in the correct direction.  He is 5'10  with black hair that is dyed semi blue. His eyes are hazel and he has a boyish look to him. He wears street style clothes all the time , not matter what he is doing. He is stylish.  He usally wears contacts, but sometimes he can be seen with thick hipster style glasses. He is determined to become a famous artist and wants to internationally recongnized, even though many of his peers say that he will never make it. He has a bit of a temper and when things start getting rough, he has the first thought of getting rough instead of thinking through things.  He draws using physical medium and worked very little with digital. 



Adrien enters the academy late into the first month. This is unusual  but does happen somtimes. He  meets korean  girl in his digital art class which forms feelings for and a strong bond. He then meets anotehr group of people along with Joan.



Korean Girl -Minah



Minah is a digital art student studing at BA. She  has been there for a month and is specialized in digital fantasy art. Her goal is to be the best webtoon artist in the world and also lead a art project for a game a studio. She is up beat, but often very baseline in emotions. Her hair is black, usually up in a pony tail with straight bangs. Sometimes you will see her wearing thick rimmed black glasses, but she doesnt need them. Only in days when she really needs to read small text or focusing on small detail will she wear them. She takes on a mentor road to Adrien, right away she sees that he lacks in core fundementals that an artist needs. Adrien also being new to digital art, she helps him navigate the tablets and programs that come with making art digital. Adrien and Minah fall in love through out the series, but the relationship is strained as Adriens mental health becomes unstable due to the horrors that he is experiencing and she is not. She believes him, but while there are times he is broken down, he recovers and comes back to normal life, making her think that he might be making it up or something is wrong with him.



Joanne



Joanne is a rich girl that attends BA looking to be a leader in business and public speaking. She is open to being a counsoler and is very orginized. Along with Adrien she expereices the same horrors along with their group. She has long blonde hair , green eyes and mediumn bust. She dresses preppy, and sometimes can be rude and stubborn, but it is never in a malice way. She see  trust adien more then the other members of her class and is the one that keep him from running away in the intro. This is more for her sake then his though. If he leaves, she does not belive that she will be able to make it. Her issues come from having parent issues and not trusting people as she has been betrayed many times even by her own family. This is why she strives to become a better bussiness perosn so she can combat her family and stand on her own two feet.



Tim

An engineering student brought to BA for working on the study of bioengineering and mechanics. He loves biology and technology and wants to fuse them to make the world into a different place. While he is a handsome guy, he suffers from low esteem and the fear of failing and not having a legacy in the world. He stands around 6 foot being one of the tallest members of the group and runs around with short brown hair that is brushed to the side. His eyes are hazel. He often dresses in a comfy sleek manner. Usually wearing a sweatshirt with all attire. He experiences the horrors along with the group, but tries to gather evidence and study the monsters that they come across. He looks up to adrien as he is able to make decisions and when he makes a decision has the confidence to go forth and do what it is. He has faith in himself and resolve to go forth to get what he wants. Which is the opposite of himself. 





Gaberial

The mysterious founder of the school. He stand well over 7 feet tall with long slender legs. Always dressed up to the nines with a top hat and a victorian style joker mask he makes his rounds in the school checking up on students and teachers. He seems to exhibit some super natural tendencies such as appearing out of know where and being able to travel to any location in an instant. His eyes also seem to change color and glow with a fire that is very  unatural. He speaks elegently and is always cheerful. He seems to take a liking to Adrien often ignoring others when he is speaking to him. He will still wave to all the students though as he passee, but his attention is soley on Adrien. Through out Adriens studies he ofetne checks up on him. Usually at night and appearing as soon as Adrien enters his room. He takes liberty of looking at all of Adriens works and even reads his diary with out his consent. His motive is unknown, but he seems to always be up to something.





