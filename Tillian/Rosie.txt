Happy Birthday Rosie~!



Somethings crawling in the attic, I think I'm going manic.

The shadow on the wall is not from me

One more shot before I panic 

Oh, none of this is  getting clearer





Black lace, white ribbons exhaled from the breasts and ruffles extended from the skirt. The dress fluttered in the autumn wind with a white knit glove holding tight to the collar. A man stepped down from a truck brushing it off. Her eyes couldn't look away. Like some trance brought upon from the textile gods she stared at the dress. A voice reached out to her and the white gloves motioned to her ears. 

"Is everything alright miss" The man smiled looking down at her. Rosie pulled her headphones off hanging them around her neck. Her eyes still transfixed on the dress.  "You want it?" This pulled her focus to him. Dressed like a fancy colonial autionair he had he striking green eyes and medium black hair with just the right amount of styling not to look messy. 

"Me? I couldn't possible take it" she shook her head " I was just admiring it from afar, that's all"  The man gave her a smile while giving a low hum. He pushed the dress even close to her, hanging it on his finger.

"I am just cleaning some old things out , I don't need it anymore, please your eyes speak for your heart. Take it before I drop it" He wagged his finger and the lace bounced up and down. Before she knew it her hands had reached out and grabbed it. White knuckled she pulled it to her chest.  She looked up to thank the man, but he was already in his truck waving good bye. Rosie made her way back to her apartment, the autumn air was cold, but the sun struck just the right balance. Pulling  the dress out in front of her and examining it more. She passed a window and draped it in front of her, it fell just below her knees and looked meant for her.  She made her way up to her apartment and opened the door. Flipping her shoes off she hung the dress on the back of a chair and plopped down on her couch. 

"Nice place" Rosie turned to be face and face with the man that had given her the dress. He nonchalantly sat with this legs crossed and resting his head on the back of his hand. She jumped up.

"What are you doing in here, you aren't supposed to be here"  she slowly backed up to the kitchen, weapons a plenty. The man took in a breath and nodded before standing. 

"You're right, In not supposed to be here, not my place" she nodded in agreement, frying pan now in hand "But, that doesn't mean I cannot be in here. You my dear girl, Rosie right? Of course. I have a proposition for you. " He made his way to dress and  picked it up again. He held it in front of her closing one of his eyes. "Looks good on your"

"Excuse me" Rosie snatched the dress away and quickly back away "You need to leave, now" The man held up his hands in surrender, but then pulled around a chair and sat down.

"How about a unique opportunity. I am owner of a certain school. A place where extremely rich parents send their bratty kids to be schooled on how to be proper adults and with some world domination involved.  All students are over 21 and not all of them are hell razors" He could tell by Rosie's expression that he was on thin ice "right, your part. In part with our on going research on the human mind and behavior we would like to have you be one of our "Test Subject"" He did air quotes for the last part. Rosie returned the frying pan to her kitchen counter and hung the dress on a open handle the returned. Her mind was still trying to figure out he had gotten into her apartment and how he knew her name. She had barely engaged back in the street. 

"What are you trying to study? and why is it in this school of yours?" Rosie crossed her arms and returned to the couch. 

"It is at the school, because I own it and the study is something I am personally involved in. Age regression and humiliation" he paused and his eyes searched "and maybe some other things, but those are the main aspects of it. Before you ask. Four years of study, you will be in one piece I insure you at the end with all things working properly and as they are now , the sweetest part." His white knit gloves gave a couple of silent claps and then a snap "Fifty million dollars"  He clapped again acting really proud of the last part. 

"Is that a guarantee?" Rosie smiled. This whole thing was ridiculous. "There are a lot of things wrong with this situation you know"

"The best times are the weirdest times. How about a trial run? One day, live in the life as my little rose"  He motioned her way "Natalie here will also be there to help" 

"Of course" A voice from behind her answered. Rosie turned to see a lady. Her hair was pulled back into a bun, square rimless classes hung on her nose and she dressed in a tight business suit.

"Were you here the whole time" Rosie asked.

"No"  Natalie gave a small shake of her head.

"Then how did you get in here? How did you all get in here?" A knock came at here door. She turned back to the man, who was rapping his knuckles against he middle her door. 

"Not all things are as they appear, or as I like to say, everything is exactly what it appears to be" He stepped aside and opened the door which swung slowly and heavily on its hinges. Rosie stepped forward to peer at what used to be the hallways to her sleepy apartment.  Like a picture, the door framed a place which was was glazed in small haze and what sounded like a saxophone playing in the background.  "Would you like to see?"  With out even sensing that she had stepped through space Rosie entered the new room. It was large, filled with dark wood. Book cases extended into the an endless black ceiling. A man sat on a chair and was indeed playing a saxophone. There was a large desk with an assortment of stationary moving on its own. Stamps pouding letters, pencils writing , and paper being folded. 

"What is this place? A haunted jazz club or something?" She circled around to the desk with all the ghost stationary.

"No, this is my office, it might just sometimes turn into a jazz club on very special occasions" He pointed to the man on stage and he left promptly giving a bow. "Well it looks like you accepted our trial" 

"What? No i haven't" Rosie spun to face the man and Natalie. " I just was amazed. We just walked through a worm hole or something, I can't just pass that up."  The man looked at his watch and then his green eyes set on her. He was older maybe in his late thirties, but when he smiled and laughed about he looked much younger. The expression though now was one of deep thought, an almost troubled look. 

"It is noon, I can't take you back till later tonight, you will have to deal with it my dear Rose. I wish I could accompany you for the beginning of your journey, but business does still need to be attended too. Natalie, keep her in decent condition till I have time" Rosie balked that she was treated like some material. She went back to the door that she had entered from. It was large, red oak with brass ornate handles. She pulled it open and not to her surprise a bunch of brooms and mops waved at her. She slammed it shut. 

"Come on, lets get you in proper attire" Natalie smiled and took Rosie by the arm. Another door lay waiting for them  at the end of a long cobblestone hallway.  As they approached the door swung open revealing the space beyond. Rosie hesitated , but with a force Natalie pulled her along. Light poured over them and Rosie had to shield her eyes when entering. It was a large domed room with a single large circle window that seemed to have a mess of gears living in it. The floor was dark wood ,still rough, but glazed. The furniture itself was not inspiring good vibes. A crib the size of a car lay against the wall directly across from the window, next to it a changing table with large diapers stuffed underneath. Passed  the changing table was a large wardrobe adorned with all sorts of fairy tale creatures carved into its wood.  Rosie stepped toward the window to take a look when the gears started to spin faster and there was a large thrum as a large metal bar moved one notch. Her eyes peered out of the window down onto a autumn forest and old buildings. She was in the schools clock tower.Natalie came to her side "You like autumn?" she questioned.

"Yeah why?" Rosie answered still staring out into the trees. 

"Master Tillian loves autumn, so the trees stay that way all year long. Students don't mind much, but some of them long for those summer nights"

"Tillian the magic man,huh?" Rosie frowned "The one day I decide to go out for a stroll, i run into someone like this"  Natalie reached behind her and pushed her toward the changing table.  She spun Rosie around to face her and gave her a shove. " This isn't really my thing" Rosie resisted. 

Then her eyes followed Natalies hand which was pointing towards the wall. There were several different sized and shaped paddles hanging by nails. "Well, maybe a sore bottom is your thing? Whether you have one or not, diapers are still part of your new uniform" Rosie decided not to push her luck.  How bad could it be? "Alright lets get those clothes off" A snap of Natalies fingers and all of Rosie's puffed into soft white clouds that slowly dissipated. Everything happened so quickly, the powder, then the diaper, followed by pastel pink and black dress.  Natalie yanked her off the table and in front of a large mirror. Large poofy shoulders with ribbons that followed the sides of the dress which ended just above her mid thigh.  Her diapers were clearly visible. Her hair was held up by two ties that looked like they had rattles adorned on them. She couldn't help it, she smiled at the ridiculousness that stood reflected. She pulled up the front of the dress to look at her diaper which was pure white and thick. It felt strange between her thighs, like she was straddling a cloud. 

"Alright, I must run for a couple of minutes, here" Natalie waved her wrist and toys sprang out from a chest near the window. Rattles, blocks,Legos and all sorts of toys sprang onto the floor like it had just rained a toy shop. "Play with this one, I think you might find it a little challenging" she held up a wooden box that had four different shaped holes on top. A star, square, triangle and a circle. The multiple colored shapes that belonged lay in the bottom of the box which slid open.  

Rosie scoffed and plopped her self down in the middle of the floor. "You know you just put the shapes through the holes right?" She slid open the drawer and pulled out each shape setting them on the floor.  Picking up the star she rolled her eyes and put it in the left whole, which was the star. The shape stopped with a wooden thump. Rosie's head snapped back down to look at the box. The star hole which was on the left was now on the right. She tried again and failed again. Star must be rigged. She picked up the triangle, but the same thing happened.  Rosie rested her head on her left hand and tapped the triangle against the box. 

"Yeah , seems really easy. I will let you figure it out. Be back in a flash" Rosie waved her off and went back to peering at the box. Every time she tried to put a shape through the correct  hole it would change.  She tired putting two shapes at the same time, then three, but nothing seemed to work. The door opened again and shut. Well that was fast.  A familiar voice called out to her. 

"How can you not figure out his dumb baby puzzle? How old are you?" Rosie pulled her self from the puzzle to come face to face with a girl. Not just any girl , it was her. Dressed in the jeans and t-shirt that she was wearing this morning. The voice seemed familiar because it was hers. She got to her feet and stared dumbfounded. Impossible was the only thought going through her mind. Her other self reached for her skirt and lifted it up a little bit "Diapers,really? I think we both know your into some other things, but your going to add this to the list" The other Rosie knelt down and perfectly put each shape in the respective hole. "Easy"

"How?

"Come on Rosie. Magic! Haven't you seen all the stuff happening here? You don't think that it wouldn't be easy to make a clone of your self with magic, hell anything is possible at this moment." A smile came over her clones lips "why don't we have a little fun then, huh little Rosie, you wanna go for a walk?" The look alike went to the wardrobe and started to toss things out onto the floor. Rosie's heart started to sink as each article landed at her feet. A dog collar, bone gag, pink leather dog paws and  ornate and cushioned knee pads.  She started to back off, but her other self started waving her hands about in the air. Each piece sought her out, wrapping and attaching with force. Rosie fell back on her diapered bottom feeling the leather tighten on her calves and back her knees. Her hands disappeared behind the paws while all protested were muzzled by the black rubber bone that made its way between her teeth. The collar wrapped  seductively around her neck, as if it was saving the pleasure of enslaving her to herself. The other Rosie made her way over pinching her cheeks then attaching a leash to pink collar now warming her neck. "You look, oh so perfect. C'mon lets go find you a proper owner" She tugged at the leash forcefully. Rosie followed clumsy with her diaper on full display and crinkling loudly with each shuffle she made. The door opened and they emerged outside in the middle of the campus below the clock tower. Students stopped, stunned by the spectacle that had just appeared before their eyes. Rosie looked up from the ground to see some of them. Instant humiliation swept over her a girl burst into laughter. If they could her cheeks would be spewing lava which would hopefully catch everything on fire, burning this whole whacked out place to the ground. She tried to keep pace with her other self, but crawling was a lot harder then she imagined. Finally they reached a bench where a young man sat writing in a journal. Her other self plopped down next to him and Rosie, panting, flopped onto the grass. Her arms couldn't take it anymore and she was out of breath.

 The young man tore his eyes from his journal and looked down at her. "Rad dog, very pretty" 

"Yeah, shes not house broken yet, but in time." The other Rosie looked down at her pitiful state then back at the young man"Five, give me a high five and shes yours" she held up her hand. Rosie couldn't take it anymore, she had to get away. She tried to get off the grass , but her arms were having none of it.  Every  muscle screamed at her out of exhaustion. Her vision started to swim as she heard "High five is pretty steep, what about a high four" She lost it and consciousness left her.

 Her head hit the ground and her eyes sprang open just as they had closed. She no longer lay on the grass out side, but sat in an old wooden school chair with the desk attached. She looked around at the classroom. Windows to the outside lined her right and windows that were fogged lined the other side of the classroom. Passed those she could see students walking back and forth. The lights were simple lanterns hanging from the ceiling and protruding from the walls. 

"Reality is a bitch sometimes"  Rosie looked toward the front of the class room. A desk sat with a large leather chair behind it. Next to it stood Tillian. He had replaced his black attire with a classic white button shirt and dark gray vest, and fitted pants. "Things can get pretty mixed up. Whats real, whats not. I deeply apologize about that. Spell gone wrong, unintended situations.

"Tillian" Rosie called. He gave a surprised look "Why didn't you tell me your name when we met?" He gave a slight chuckle

"No reason to get attached so quickly, I was prepared for you to call me by other names for a while" Tillian stared off into the distance like he was thinking of a back up plan then spoke again "No matter. Rosie, my name is Tillain , pleasure to meet you" He reached out his hand for a handshake. Instinctively Rosie got up to go shake it , but when she stood up warm liquid slithered down her legs which her white socks soaked up until it reached her Velcro shoes. Between her legs and under her short sailor skirt was an extremely full and leaky diaper. She lifted her skirt in disgust and embarrassment.  Her eyes went to Tillain who was still extending his hand with a smirk on his face. He retracted his hand and rubbed his chin "Oh dear, looks like someone has had one too many accidents" He took a piece of chalk and wrote seven followed by two zeros. "Here, let me explain this, in seven minutes this class will be in session. You have two choices. Stand or sit their like a very naughty two year old or get your diaper changed by me"  With the strike of two dots for the semi-colon, the chalk numbers started to count down. Rosie took a moment to process the situation she had been placed in. She made her way to the front of the class room her sneakers squeaked loudly and urine dripping on to the stone floor. 

Mortified she looked up at Tillian ."Please can you change me"  His hand went to the front of her diaper and gave it a squeeze. He tsked then gave a long sigh. 

"Rosie , aren't you a little too old for diapers? What do you have to say for yourself" Her cheeks flushed and she tried to explain. She didn't even know what had happened "hm? letting it get this far, how irresponsible of you, now explain to me what you want" 

"I need a diaper change" Tillian raised his eyebrows.

"whats the magic word?" 

"Please" She whispered and looked back up at the chalk clock. Two minutes had already passed.

"Who's diaper needs changing" 

"Mine!" she yelled "I need my diaper changed, please" tears stung her eyes and she gritted her teeth.

"Of course my lady ,come on lets get you up here" With a wave of Tillians wrist a blanket appeared on the desk and he guided her gentle down onto it. A full pastel diaper bag burst into reality in the leather chair. "lets get all these wet things off shall we" He uncorked her shoes and peeled off her soaked socks immediately throwing them into the air where each faded into golden dust and rained down onto them. Rosie peered at the clock again. Another minute had passed.  She wanted to tell him to hurry up, but her voice was caught in her throat. Tillian pulled off his white knit gloves one finger at a time and set them on the back of the chair. He undid the tapes of her sodden diaper and with a motion her legs lifted into that air.  There was sudden coolness that struck her again today when he removed it. Still her legs remained in the air. She tried to get them back down to the desk or at least put her legs together, but something held the completely spread apart and in the air above her head.  She couldn't close her eyes for more then a moment and even her hands seemed to be nailed to the desk. Next he used a warm wipe to clean her whole lower half including her legs. His hand moved like a feather up and down her thighs. He looked over at her with a smile "You have lovely skin my little Rose, we must keep it healthy"  Then he danced his hands above her and powder snowed down from an invisible cloud. It collected in banks upon her legs and navel. A twinge of excitement surged  as his hands worked powder with a gentle firmness into inch of her. Next and finally was her diaper. Another thick, white disposable diaper was unfolded and placed below her hips which we now allowed to rest upon the desk again. It was almost over Rosie smiled. Then the bell rang. Her sudden happiness dashed into pieces as she tilted her head back to see the sliding door of the class room open. The high four kid stood in the door way blocked the rest of his classmates which were all protesting. 

Tillian waved them in "Come on in, I am just finishing up here"  The students filed into the classroom each one taking in the full sight of her. Rosie turned her head and stared at the black board. Her heart beat filled her ears and  her body skyrocketed to sun like temperatures.  She had completely forgotten about the time. Beyond her heart beat she could her the giggling of some of the students. Tillian finished taping her up and pulled a set of white tights up her legs and shimmied them up above her diaper. Another set of Velco shoes were placed upon her feet "Say hello the class Rosie, don't be rude" Against her will she swung her legs off the desk and hopped off in front of them. She curtsied and as she did the black dress she had taken this morning weaved its self in to her hands and onto her. 

"Hello, my name is Rosie. It is a pleasure to meet all of you"  Her face was a forced smile by what ever magic flowed through this place. Her eyes darted to all the different faces. Some laughed at her, other were taking pictures with phones and some feeling embarrassed for her turned their heads away. 

The door slid open again and Natalie burst through out of breath and panting. "Whoa, sorry I am late, class, was busy looking for" She looked up hands on knees and pointed to Rosie "her"

Tillian patted Natalie's back "Don't worry we were just having a good time, we must be leaving now, isn't that right Rosie, come on" He  walked up and patted her thick behind.  They walked out of the class room and then through the old English hallways to outside.  As soon as they stepped outside Rosie had control of her body back.

It was already night outside and the moon was full which illuminated everything with a silvery brightness.  They walked until they reached where all the cobbled paths met in the middle of the campus. He swung around on his heels and reached toward the ground. His hands caught something and he flipped a table and chairs into existence. As his chair swirled on its legs he caught it and melted into it. Again he reached out his hand, but this time for her to sit. "No" Rosie heard her self say as she turned around and started to walk away. She walked,but Tillian and his desk kept showing up. Every time she turned it lead back to the middle of the paths with Tillian still smiling and waiting for her to take a seat. She couldn't take it, there was only one way of getting out. She took a seat in front of him. she shifted a couple of times to find her balance. The diaper was so thick that she barely could feel the chair.  His face turned more serious as she straightened her chair to square off. 

"Questions, you have them and I will answer them, I know I haven't been too transparent with you Rosie, but I will be"  He sat up and leaned forward. 

"What are you?" 

Tillian cleared his throat "A lesser god. I pissed off death on my way to the great afterlife and got demoted to a human form" 

"Why are you doing this to me?" 

"You walked through the door" he answered quickly

Rosie crossed her arms "Everyone would walk through, did you see what you did? Defying space time, that's incredible" she looked down at the table. A lantern flickered against a stretched piece of parchment . She looked up to meet his eyes which glowed piercing green against he darkness.  He was powerful and she had witnessed what he could do today.

"Why not by force, you don't need me to sign this for you to do anything"

"Forcing people to do things is not for me. Besides, your the first to walk through the door."  Leaning forward she reached for the contract. Could she do four years of this? Money was great, but she was fine with earning what she made now. She bit her lip. Maybe it wouldn't be so bad. When she reached down to feel the front of her diaper her heart beat skipped. The chair she sat in fell back as she stood up. 

"A couple conditions. One - You teach me anything I want to learn, two - I get to keep my memory. No point in gaining skills and not remembering how to do them and three - you have to run everything your going to do to me before hand and I get to choose what flies or not." Tillian waved his hands above the contract. The ink swirled and formed the conditions. She eyed him.

"Don't worry, once it is signed I can't change it. Are you sure about this Rosie?" Tillian still peered at her with a doubtful face. She looked up from the contract with a smile. 

"Yes, the best times are the weirdest times"  The pen scratched into the paper and her name bled into it. Tillian let out a sigh of relief and gave a large smile "Now" Rosie thrust her first into her hips "teach me how  to control you"

  











